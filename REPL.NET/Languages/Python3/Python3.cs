﻿using REPLDOTNET.Connections;
using REPLDOTNET.Languages.Python3.DotNetToPythonTypeConvertors;
using REPLDOTNET.Languages.Python3.PythonToDotNetTypeConvertors;
using System;
using System.Collections.Generic;

namespace REPLDOTNET.Languages.Python3;

public class Python3 : IReplLanguage
{
    public Python3()
    {
        _dotNetToReplTypeConvertors.Add(typeof(string), new StringToPythonConvertor());
        
        _replToDotNetTypeConvertors.Add(typeof(string), new PythonToStringConvertor());
    }
    private readonly Dictionary<Type, IDotNetToReplTypeConvertor> _dotNetToReplTypeConvertors = new Dictionary<Type, IDotNetToReplTypeConvertor>();
    private readonly Dictionary<Type, IReplToDotNetTypeConvertor> _replToDotNetTypeConvertors = new Dictionary<Type, IReplToDotNetTypeConvertor>();
    public string LanguageRuntimeExe { get; } = "python3";
    //interactive mode and unbuffered output.
    public string LanguageRuntimeExeParameters { get; } = "-i, -u";
    public string Prompt { get; } = ">>>";
    public string Name { get; } = "Python3";
    public string MethodFormat { get; } = "{0}({1})";
    public string ArgumentSeparator { get; } = ", ";
    public T ConvertFromReplType<T>(string aValue)
    {
        var convertor = _replToDotNetTypeConvertors[typeof(T)];
        return (T)convertor.Convert(aValue);
    }
    public string ConvertToReplType<T>(T aValue)
    {
        var convertor = _dotNetToReplTypeConvertors[typeof(T)];
        return convertor.Convert(aValue);
    }
    public void AddReplToDotNetTypeConvertor(IReplToDotNetTypeConvertor aValue)
    {
        if (aValue == null)
            throw new ArgumentNullException(nameof(aValue));

        _replToDotNetTypeConvertors.Add(aValue.TargetType, aValue);
    }
    public void AddDotNetTypeToReplConvertor(IDotNetToReplTypeConvertor aValue)
    {
        if (aValue == null)
            throw new ArgumentNullException(nameof(aValue));

        _dotNetToReplTypeConvertors.Add(aValue.SourceType, aValue);
    }
    public string SanitizeReturnValue(string aValue)
    {
        return aValue;
    }
}