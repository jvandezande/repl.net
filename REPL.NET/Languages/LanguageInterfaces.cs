﻿using REPLDOTNET.Connections;
using System;

namespace REPLDOTNET.Languages;

/// <summary>
///     Represents a REPL (Read-Eval-Print Loop) language interface that provides language-specific functionality,
///     type conversion, and metadata for interaction within a REPL environment.
/// </summary>
public interface IReplLanguage
{
    /// <summary>
    /// The name of the executable file for the runtime environment of the language.
    /// </summary>
    public string LanguageRuntimeExe { get; }
    /// <summary>
    /// Specifies the command-line parameters to be passed to the runtime environment of the language.
    /// </summary>
    public string LanguageRuntimeExeParameters { get; }
    /// <summary>
    ///     This the string that is used to identify a prompt string.
    ///     e.g. Python REPL is >>>
    /// </summary>
    public string Prompt { get; }
    /// <summary>
    ///     Gets the name of the REPL language used to identify the specific programming language implementation.
    /// </summary>
    public string Name { get; }
    /// <summary>
    ///     Specifies the format string for constructing method calls in the REPL language.
    ///     This determines how functions or methods are represented and invoked within the REPL environment.
    ///     e.g. for Python this is "{0}({1})" where {0} is the method name and {1} are the parameters
    /// </summary>
    public string MethodFormat { get; }
    /// <summary>
    ///     The string used to separate arguments in method calls or commands
    ///     within the REPL language context.
    /// </summary>
    public string ArgumentSeparator { get; }
    /// <summary>
    ///     Converts a given string value from a REPL-specific type to the specified type.
    /// </summary>
    /// <typeparam name="T">The target type to which the string value will be converted.</typeparam>
    /// <param name="aValue">The string value to be converted.</param>
    /// <returns>The converted value of the specified type.</returns>
    public T ConvertFromReplType<T>(string aValue);
    /// <summary>
    ///     Converts a given value of the specified type to a REPL-specific string representation.
    /// </summary>
    /// <typeparam name="T">The type of the value to be converted.</typeparam>
    /// <param name="aValue">The value to be converted to a REPL-specific string.</param>
    /// <returns>A string representation of the value in REPL-specific format.</returns>
    public string ConvertToReplType<T>(T aValue);
    /// <summary>
    ///     Adds a type convertor for converting REPL-specific string representations to .NET types.
    /// </summary>
    /// <param name="aValue">
    ///     An implementation of the interface handling the conversion from REPL-specific strings to .NET
    ///     types.
    /// </param>
    public void AddReplToDotNetTypeConvertor(IReplToDotNetTypeConvertor aValue);
    /// <summary>
    ///     Adds a .NET to REPL type convertor for a specified .NET type.
    /// </summary>
    /// <param name="aValue">The convertor that converts a .NET type to a REPL-specific string representation.</param>
    public void AddDotNetTypeToReplConvertor(IDotNetToReplTypeConvertor aValue);
    /// <summary>
    ///     Sanitizes and processes the given string value before it is returned by the REPL.
    /// </summary>
    /// <param name="aValue">The string value to be sanitized.</param>
    /// <returns>A sanitized version of the string, in case of errors this method can throw exceptions</returns>
    public string SanitizeReturnValue(string aValue);
}

/// <summary>
///     Defines a converter interface responsible for translating REPL (Read-Eval-Print Loop) string representations
///     of data into their corresponding .NET data types.
/// </summary>
public interface IReplToDotNetTypeConvertor
{
    /// <summary>
    ///     Represents the .NET type to which a REPL-specific value will be converted.
    /// </summary>
    public Type TargetType { get; }
    /// <summary>
    ///     Converts a given value between different formats or types based on implementation logic.
    /// </summary>
    /// <typeparam name="T">The target type to which the input value will be converted.</typeparam>
    /// <param name="aValue">The input value to be converted.</param>
    /// <returns>The converted value as the specified type.</returns>
    public object Convert(string aValue);
}

/// <summary>
///     Defines a converter interface responsible for transforming .NET data types
///     into their corresponding string representations for use within a REPL (Read-Eval-Print Loop) environment.
/// </summary>
public interface IDotNetToReplTypeConvertor
{
    /// <summary>
    ///     Represents the source .NET type that this converter translates to or from REPL string representations.
    /// </summary>
    public Type SourceType { get; }
    /// <summary>
    ///     Converts a specified value to a different representation based on the implemented conversion logic.
    /// </summary>
    /// <typeparam name="T">The target type for the conversion.</typeparam>
    /// <param name="value">The value to be converted.</param>
    /// <returns>The converted value as the target type.</returns>
    public string Convert(object aValue);
}