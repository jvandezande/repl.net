using System.Collections.Generic;

namespace REPLDOTNET;

public static class StringExtensions
{
    public static IEnumerable<string> Slice(this string s, int chunkSize)
    {
        var stringLength = s.Length;
        for (var i = 0; i < stringLength; i += chunkSize)
        {
            if (i + chunkSize > stringLength)
            {
                chunkSize = stringLength - i;
            }

            yield return s.Substring(i, chunkSize);
        }
    }
}