﻿using System;

namespace REPLDOTNET.Languages.Python3.PythonToDotNetTypeConvertors;

public class PythonToStringConvertor: IReplToDotNetTypeConvertor
{

    public Type TargetType { get; } = typeof(string);
    public object Convert(string aValue)
    {
        return aValue;
    }
}