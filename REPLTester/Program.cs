﻿using REPLDOTNET;
using REPLDOTNET.Connections;
using REPLDOTNET.Languages.Python3;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace REPLTester;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Start");
        var repl = new REPL();
        var sw = new Stopwatch();
        sw.Start();
        var conn = new LocalConnection("C:\\Users\\Jeroe\\PycharmProjects\\PythonProject1");
        repl.Connection = conn;
        repl.Language = new Python3();
        Console.WriteLine(sw.Elapsed);


        var greet = repl.CreateFunctionCall<string, string>("greet");
        var reverse_string = repl.CreateFunctionCall<string, string>("reverse_string");
        var list_sum = repl.CreateFunctionCall<IEnumerable<double>, double>("list_sum");

        //var somePythonFunction2 = repl.CreatePythonFunctionCall<Double>("(25*4)+50.0");
        //var somePythonFunction3 = repl.CreatePythonFunctionCall<Double>("(5*4.0)");

        repl.CreateReplCall("from repltest import *")();

        //Console.WriteLine(somePythonFunction3());
        //Console.WriteLine(somePythonFunction2());
        Console.WriteLine(greet("world"));
        Console.WriteLine(reverse_string("reverse me"));
        Console.WriteLine(list_sum([1.0, 2.0, 3.0, 4.0, 5.0]));
        //Console.WriteLine(somePythonFunction3());
        //Console.WriteLine(somePythonFunction2() + somePythonFunction2());

        sw.Stop();
        Console.WriteLine(sw.Elapsed);
    }
}