using REPLDOTNET.Connections;
using REPLDOTNET.Languages;
using System;
using System.Globalization;
using System.Threading;

namespace REPLDOTNET;

public class REPL : IDisposable
{

    public IConnection Connection { get; set; }

    public IReplLanguage Language { get; set; }

    public string NewLine { get; set; } = Environment.NewLine;

    public CultureInfo Culture { get; set; } = CultureInfo.InvariantCulture;

    private string ExecuteExpression(string aCommand)
    {
        ExecuteNoFollow(aCommand);
        return Follow(10);
    }

    private void ExecuteNoFollow(string aCommand)
    {
        if (aCommand.Length < 256)
        {
            Connection.Write(aCommand);
        }
        else
        {
            foreach (var s in aCommand.Slice(256))
            {
                Connection.Write(s);
                Thread.Sleep(10);
            }
        }
        Connection.Write(NewLine);
    }

    private string Follow(int aTimeoutInSeconds)
    {
        // wait for normal output
        var data = Connection.ReadUntilPrompt(Language.Prompt, aTimeoutInSeconds);

        data = data.Trim();

        /*if (data.Contains("NameError: "))
        {
            throw new UndefinedMethodREPLException(data);
        }
        if (data.Contains("TypeError: "))
        {
            throw new InvalidArgumentsREPLException(data);
        }*/
        data = Language.SanitizeReturnValue(data);
        return data;
    }

    public Func<TResult> CreateFunctionCall<TResult>(string command)
    {
        if (String.IsNullOrEmpty(command))
        {
            throw new ArgumentException($"{nameof(command)} is null or empty.", nameof(command));
        }

        return delegate { return Language.ConvertFromReplType<TResult>(ExecuteExpression(command)); };
    }


    public Func<TParam1, TResult> CreateFunctionCall<TParam1, TResult>(string command)
    {
        if (String.IsNullOrEmpty(command))
        {
            throw new ArgumentException($"{nameof(command)} is null or empty.", nameof(command));
        }

        return arginput =>
        {
            return Language.ConvertFromReplType<TResult>(ExecuteExpression(String.Format(Language.MethodFormat, command, Language.ConvertToReplType(arginput))));
        };
    }

    private string _concatinateArgumentHelper(string[] args)
    {
        return String.Join(Language.ArgumentSeparator, args);
    }

    public Func<TParam1, TParam2, TResult> CreateFunctionCall<TParam1, TParam2, TResult>(string command)
    {
        if (String.IsNullOrEmpty(command))
        {
            throw new ArgumentException($"{nameof(command)} is null or empty.", nameof(command));
        }

        return (arginput1, arginput2) =>
        {
            var args = _concatinateArgumentHelper(new[]
            {
                Language.ConvertToReplType(arginput1), Language.ConvertToReplType(arginput2)
            });
            return Language.ConvertFromReplType<TResult>(ExecuteExpression(String.Format(Culture, Language.MethodFormat, command, args)));
        };
    }

    public Action CreateReplCall(string command)
    {
        if (String.IsNullOrEmpty(command))
        {
            throw new ArgumentException($"{nameof(command)} is null or empty.", nameof(command));
        }

        return delegate { ExecuteExpression(command); };
    }

    #region IDisposable Support

    private bool disposedValue;

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                Connection.Dispose();
            }
            disposedValue = true;
        }
    }

    // This code added to correctly implement the disposable pattern.
    public void Dispose()
    {
        // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        Dispose(true);
    }

    #endregion

}