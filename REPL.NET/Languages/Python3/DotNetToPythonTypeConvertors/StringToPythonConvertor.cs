﻿using System;

namespace REPLDOTNET.Languages.Python3.DotNetToPythonTypeConvertors;

public class StringToPythonConvertor : IDotNetToReplTypeConvertor
{

    public Type SourceType { get; } = typeof(string);
    public string Convert(object aValue)
    {
        if (aValue is not string stringValue)
        {
            throw new ArgumentException("Value must be of type string.", nameof(aValue));
        }

        // Escape special characters for Python representation and wrap it in quotes
        // For example: "Hello\nWorld" -> "\"Hello\\nWorld\""
        return $"\"{stringValue.Replace("\\", "\\\\").Replace("\"", "\\\"")}\"";
    }
}