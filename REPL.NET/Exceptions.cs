﻿using System;

namespace REPLDOTNET;

public class REPLException : Exception
{
    public REPLException(string message)
        : base(message)
    {
    }
}

public class UndefinedMethodREPLException : REPLException
{
    public UndefinedMethodREPLException(string message)
        : base(message)
    {
    }
}

public class InvalidArgumentsREPLException : REPLException
{
    public InvalidArgumentsREPLException(string message)
        : base(message)
    {
    }
}