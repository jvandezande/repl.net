﻿using REPLDOTNET.Languages;
using System;

namespace REPLDOTNET.Connections;

public interface IConnection : IDisposable
{
    string ReadUntilPrompt(string prompt, int aTimeOutInSeconds = 30);
    void Write(string aData);
    void Open(IReplLanguage aLanguage);
    void Close();
}