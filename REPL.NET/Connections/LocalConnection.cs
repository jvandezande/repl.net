﻿using REPLDOTNET.Languages;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace REPLDOTNET.Connections;

public class LocalConnection : IConnection, IDisposable
{
    private readonly Process _process;
    private readonly StringBuilder _stderrBuffer = new StringBuilder();
    private readonly object _stderrLock = new object();
    private readonly StringBuilder _stdoutBuffer = new StringBuilder();
    private readonly object _stdoutLock = new object();


    public LocalConnection(string aWorkingDirectory = null)
    {
        var psi = new ProcessStartInfo()
        {
            RedirectStandardInput = true,
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            UseShellExecute = false,
            CreateNoWindow = true,
            WorkingDirectory = aWorkingDirectory
        };

        _process = new Process
        {
            StartInfo = psi,
            EnableRaisingEvents = true
        };

        _process.Start();

        Task.Run(() => ReadStream(_process.StandardOutput, _stdoutBuffer, _stdoutLock));
        Task.Run(() => ReadStream(_process.StandardError, _stderrBuffer, _stderrLock));

    }

    public void Dispose()
    {
        _process?.Kill();
        _process?.Dispose();
    }


    public string ReadUntilPrompt(string aPrompt, int timeoutInSeconds = 10)
    {
        return _ReadUntilPromptThreadSafe(aPrompt, timeoutInSeconds);
    }

    public void Write(string command)
    {
        if (_process.HasExited)
            throw new InvalidOperationException("The REPL process has exited.");

        _process.StandardInput.WriteLine(command);
        _process.StandardInput.Flush();
    }
    public void Open(IReplLanguage aLanguage)
    {
        throw new NotImplementedException();
    }
    public void Close()
    {
        throw new NotImplementedException();
    }

    private void ReadStream(StreamReader reader, StringBuilder buffer, object lockObject)
    {
        var chunk = new char[256];
        int count;
        while (!_process.HasExited)
        {
            count = reader.Read(chunk, 0, chunk.Length);
            lock (lockObject)
            {
                buffer.Append(chunk, 0, count);
            }
        }
    }

    private string _ReadUntilPromptThreadSafe(string aPrompt, int timeoutInSeconds = 30)
    {
        var timeout = TimeSpan.FromSeconds(timeoutInSeconds);
        var startTime = DateTime.UtcNow;
        var tempBuffer = new StringBuilder();
        Thread.Sleep(500);
        while (DateTime.UtcNow - startTime < timeout)
        {
            lock (_stdoutLock)
            {
                // Check if the prompt is present in stdout
                var promptIndex = _stdoutBuffer.ToString().IndexOf(aPrompt, StringComparison.InvariantCulture);

                if (promptIndex >= 0)
                {
                    // Capture stdout data up to the prompt (exclude the prompt itself)
                    tempBuffer.Append(_stdoutBuffer.ToString(0, promptIndex));
                    _stdoutBuffer.Remove(0, promptIndex + aPrompt.Length); // Remove prompt
                    Thread.Sleep(1);

                    // Capture all current stderr data
                    lock (_stderrLock)
                    {
                        tempBuffer.Append(_stderrBuffer.ToString());
                        _stderrBuffer.Clear();
                    }

                    // Return stderr data (stdout processing is complete)
                    return tempBuffer.ToString();
                }
            }

            lock (_stderrLock)
            {
                // Check if the prompt is present in stderr
                var promptIndex = _stderrBuffer.ToString().IndexOf(aPrompt, StringComparison.InvariantCulture);

                if (promptIndex >= 0)
                {
                    // Capture stderr data up to the prompt (exclude the prompt itself)
                    tempBuffer.Append(_stderrBuffer.ToString(0, promptIndex));
                    _stderrBuffer.Remove(0, promptIndex + aPrompt.Length); // Remove prompt
                    Thread.Sleep(1);

                    // Capture all current stdout data
                    lock (_stdoutLock)
                    {
                        tempBuffer.Append(_stdoutBuffer.ToString());
                        _stdoutBuffer.Clear();
                    }

                    // Return stderr data (stderr processing is complete)
                    return tempBuffer.ToString();
                }
            }

            // If no prompt found, wait briefly for more data
            Thread.Sleep(1);
        }

        throw new REPLException("Could not find REPL prompt");
    }
}